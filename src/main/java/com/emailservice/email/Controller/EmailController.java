package com.emailservice.email.Controller;



import com.amazonaws.services.simpleemail.model.SendEmailResult;
import com.amazonaws.services.simpleemail.model.VerifyEmailAddressResult;
import com.emailservice.email.Model.CustomErrorMessage;
import com.emailservice.email.Model.Email;
import com.emailservice.email.Model.ReportAndEmail;
import com.emailservice.email.Service.ServiceImpl;
import com.emailservice.email.Verify.VerifyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@RestController
public class EmailController {
    @Autowired
    ServiceImpl service;


    @GetMapping("/greetings")
    public ResponseEntity<?> greetings() {
        return new ResponseEntity<>("hello world", HttpStatus.OK);
    }


    @PostMapping("/sendEmail")
    public ResponseEntity<?> sendEmail(@RequestBody Email email) {
        CustomErrorMessage errorMessage = VerifyUtil.verifyEmailContents(email);
        if(errorMessage != null) {
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
        SendEmailResult sendEmailResult = service.sendEmail(email);
        if(sendEmailResult == null) {
            errorMessage = new CustomErrorMessage();
            errorMessage.setErrorMessage("Unable to send email " + email.toString());
            errorMessage.setStatusCode(400);
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(sendEmailResult, HttpStatus.OK);
    }

    @PostMapping("/generateReport")
    public ResponseEntity<?> generateReport(@RequestBody ReportAndEmail reports) {
        CustomErrorMessage customErrorMessage = service.generateAndSendReport(reports);
        if(customErrorMessage != null) {
            return new ResponseEntity<>(customErrorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        //File is written, so now we send it to the email.

        return new ResponseEntity<>(reports, HttpStatus.OK);
    }

    @PostMapping("/verifyEmails")
    public ResponseEntity<?> verifyEmail(@RequestBody List<String> emails) {
        List<VerifyEmailAddressResult> results = new ArrayList<>();
        for(String email : emails) {
            results.add(service.verifyEmail(email));
        }
    return new ResponseEntity<>(results, HttpStatus.OK);
    }



}
