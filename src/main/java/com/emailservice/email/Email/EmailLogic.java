package com.emailservice.email.Email;


import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.*;
import com.emailservice.email.Constants.Constants;
import com.emailservice.email.Model.CustomErrorMessage;
import com.emailservice.email.Model.Email;
import com.emailservice.email.Model.Report;
import com.emailservice.email.Model.ReportAndEmail;
import com.emailservice.email.Report.ReportLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
//import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Properties;

@Component
public class EmailLogic {

    AmazonSimpleEmailService client;

    @Autowired
    ReportLogic reportLogic;

    public EmailLogic() {
        this.client =  AmazonSimpleEmailServiceClientBuilder.standard().withRegion(Regions.US_WEST_2).build();
    }


    public VerifyEmailAddressResult emailToVerify(String email) {
        VerifyEmailAddressResult result = null;
        VerifyEmailAddressRequest verifyEmailAddressRequest = new VerifyEmailAddressRequest();
        verifyEmailAddressRequest.setEmailAddress(email);
        try {
           result = this.client.verifyEmailAddress(verifyEmailAddressRequest);
        } catch(Exception ex) {
            System.out.println("Unable to verify email, Error Message: " + ex.getMessage());
        }
        return result;
    }



    public SendEmailResult sendEmail(Email email) {
        SendEmailResult result = null;
        try {
            SendEmailRequest request = new SendEmailRequest()
                    .withDestination(
                            new Destination().withToAddresses(email.getToEmail()))
                    .withMessage(new Message()
                            .withBody(new Body()
                                    .withText(new Content()
                                            .withCharset("UTF-8").withData(email.getTextBody())))
                            .withSubject(new Content()
                                    .withCharset("UTF-8").withData(email.getSubject())))
                    .withSource(email.getFromEmail());
                    // Comment or remove the next line if you are not using a
                    // configuration set
            result = client.sendEmail(request);
        } catch (Exception ex) {
            System.out.println("The email was not sent. Error message: "
                    + ex.getMessage());
        }
        return result;
    }

    public CustomErrorMessage sendEmailWithReport(ReportAndEmail reportAndEmail) {
        CustomErrorMessage customErrorMessage = reportLogic.generateReport(reportAndEmail.getReports());
        if(customErrorMessage!= null) {
            return customErrorMessage;
        }
        Session session = Session.getDefaultInstance(new Properties());
        MimeMessage message = new MimeMessage(session);
        try {
            Email email = reportAndEmail.getEmails();
            message.setSubject(email.getSubject(), "UTF-8");
            message.setFrom(new InternetAddress(email.getToEmail()));
            message.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(email.getToEmail()));
            MimeMultipart msg_body = new MimeMultipart("alternative");
            MimeBodyPart wrap = new MimeBodyPart();
            MimeBodyPart textPart = new MimeBodyPart();
            textPart.setContent(email.getTextBody(), "text/plain; charset=UTF-8");
            msg_body.addBodyPart(textPart);
            wrap.setContent(msg_body);
            MimeMultipart msg = new MimeMultipart("mixed");
            message.setContent(msg);
            msg.addBodyPart(wrap);
            MimeBodyPart att = new MimeBodyPart();
            DataSource fds = new FileDataSource(Constants.temporaryFile);
            att.setDataHandler(new DataHandler(fds));
            att.setFileName(fds.getName());
            msg.addBodyPart(att);
            System.out.println("Attempting to send an email through Amazon SES "
                    +"using the AWS SDK for Java...");
            AmazonSimpleEmailService client =
                    AmazonSimpleEmailServiceClientBuilder.standard()
                            .withRegion(Regions.US_WEST_2).build();
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            message.writeTo(outputStream);
            RawMessage rawMessage =
                    new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));

            SendRawEmailRequest rawEmailRequest =
                    new SendRawEmailRequest(rawMessage);

            client.sendRawEmail(rawEmailRequest);
            System.out.println("Email sent!");
        } catch(MessagingException msg) {
            customErrorMessage = new CustomErrorMessage();
            customErrorMessage.setErrorMessage(msg.getMessage());
            customErrorMessage.setStatusCode(500);
            return customErrorMessage;
        } catch(IOException io) {
            customErrorMessage = new CustomErrorMessage();
            customErrorMessage.setErrorMessage(io.getMessage());
            customErrorMessage.setStatusCode(500);
            return customErrorMessage;


        }

        return null;
    }




}
