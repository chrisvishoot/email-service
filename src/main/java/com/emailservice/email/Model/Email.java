package com.emailservice.email.Model;

public class Email {

    private String fromEmail;
    private String toEmail;
    private String subject;
    private String htmlBody;
    private String textBody;

    public Email() {

    }


    public String getFromEmail() {
        return fromEmail;
    }

    public Email setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
        return this;
    }

    public String getToEmail() {
        return toEmail;
    }

    public Email setToEmail(String toEmail) {
        this.toEmail = toEmail;
        return this;
    }

    public String getSubject() {
        return subject;
    }

    public Email setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public String getHtmlBody() {
        return htmlBody;
    }

    public Email setHtmlBody(String htmlBody) {
        this.htmlBody = htmlBody;
        return this;
    }

    public String getTextBody() {
        return textBody;
    }

    public Email setTextBody(String textBody) {
        this.textBody = textBody;
        return this;
    }

    @Override
    public String toString() {
        return "Email{" +
                "fromEmail='" + fromEmail + '\'' +
                ", toEmail='" + toEmail + '\'' +
                ", subject='" + subject + '\'' +
                ", htmlBody='" + htmlBody + '\'' +
                ", textBody='" + textBody + '\'' +
                '}';
    }
}
