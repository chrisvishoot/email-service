package com.emailservice.email.Model;

public class Report {
    private Long reportId;

    private Long numberOfContacts;
    private Long promisedToGo;

    private Long telegramUserId;
    private String timeStamp;

    private Long idTelegramUser;
    private String telegramUserName;

    public Report() {}

    public Long getReportId() {
        return reportId;
    }

    public Report setReportId(Long reportId) {
        this.reportId = reportId;
        return this;
    }

    public Long getNumberOfContacts() {
        return numberOfContacts;
    }

    public Report setNumberOfContacts(Long numberOfContacts) {
        this.numberOfContacts = numberOfContacts;
        return this;
    }

    public Long getPromisedToGo() {
        return promisedToGo;
    }

    public Report setPromisedToGo(Long promisedToGo) {
        this.promisedToGo = promisedToGo;
        return this;
    }

    public Long getTelegramUserId() {
        return telegramUserId;
    }

    public Report setTelegramUserId(Long telegramUserId) {
        this.telegramUserId = telegramUserId;
        return this;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public Report setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
        return this;
    }

    public Long getIdTelegramUser() {
        return idTelegramUser;
    }

    public Report setIdTelegramUser(Long idTelegramUser) {
        this.idTelegramUser = idTelegramUser;
        return this;
    }

    public String getTelegramUserName() {
        return telegramUserName;
    }

    public Report setTelegramUserName(String telegramUserName) {
        this.telegramUserName = telegramUserName;
        return this;
    }

    @Override
    public String toString() {
        return "Report{" +
                "reportId=" + reportId +
                ", numberOfContacts=" + numberOfContacts +
                ", promisedToGo=" + promisedToGo +
                ", telegramUserId=" + telegramUserId +
                ", timeStamp='" + timeStamp + '\'' +
                ", idTelegramUser=" + idTelegramUser +
                ", telegramUserName='" + telegramUserName + '\'' +
                '}';
    }
}
