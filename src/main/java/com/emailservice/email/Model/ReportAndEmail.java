package com.emailservice.email.Model;

import java.util.List;

public class ReportAndEmail {
    private List<Report> reports;
    private Email emails;

    public ReportAndEmail() {}

    public List<Report> getReports() {
        return reports;
    }

    public ReportAndEmail setReports(List<Report> reports) {
        this.reports = reports;
        return this;
    }

    public Email getEmails() {
        return emails;
    }

    public ReportAndEmail setEmails(Email emails) {
        this.emails = emails;
        return this;
    }

    @Override
    public String toString() {
        return "ReportAndEmail{" +
                "reports=" + reports +
                ", emails=" + emails +
                '}';
    }
}
