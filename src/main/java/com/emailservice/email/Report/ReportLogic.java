package com.emailservice.email.Report;

import com.emailservice.email.Constants.Constants;
import com.emailservice.email.Model.CustomErrorMessage;
import com.emailservice.email.Model.Report;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ReportLogic {
    Map<Long, Report> uniqueUsers;
    String[] headers = {"Full Name", "Time Stamp", "Number Contacts", "Promised to Go"};


    ReportLogic() {
        uniqueUsers = new HashMap<>();
    }

    public CustomErrorMessage generateReport(List<Report> reports) {
        CustomErrorMessage customErrorMessage = null;
        for(Report report : reports) {
            if(uniqueUsers.containsKey(report.getIdTelegramUser())) {
                Report currentReport = uniqueUsers.get(report.getIdTelegramUser());
                long numberOfContacts = currentReport.getNumberOfContacts();
                numberOfContacts += report.getNumberOfContacts();
                currentReport.setNumberOfContacts(numberOfContacts);
                uniqueUsers.put(report.getIdTelegramUser(), currentReport);
            } else {
                uniqueUsers.put(report.getIdTelegramUser(), report);
            }
        }
        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(Constants.temporaryFile));
            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader(headers));

            for(Long userId : uniqueUsers.keySet()) {
                Report uniqueReport = uniqueUsers.get(userId);
                csvPrinter.printRecord(uniqueReport.getTelegramUserName(),
                        uniqueReport.getTimeStamp(),
                        uniqueReport.getNumberOfContacts(),
                        uniqueReport.getPromisedToGo());
            }
            csvPrinter.flush();
        } catch(IOException ex) {
            System.out.println(ex);
            customErrorMessage = new CustomErrorMessage();
            customErrorMessage.setStatusCode(500);
            customErrorMessage.setErrorMessage(ex.getMessage());
        }
        return customErrorMessage;

    }

}
