package com.emailservice.email.Service;

import com.amazonaws.services.simpleemail.model.SendEmailResult;
import com.amazonaws.services.simpleemail.model.VerifyEmailAddressResult;
import com.emailservice.email.Model.CustomErrorMessage;
import com.emailservice.email.Model.Email;
import com.emailservice.email.Model.Report;
import com.emailservice.email.Model.ReportAndEmail;

import java.util.List;

public interface Service {
    public SendEmailResult sendEmail(Email email);
    public VerifyEmailAddressResult verifyEmail(String emailToVerify);
    public CustomErrorMessage generateAndSendReport(ReportAndEmail reportAndEmail);

}
