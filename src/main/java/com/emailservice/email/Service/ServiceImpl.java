package com.emailservice.email.Service;

import com.amazonaws.services.simpleemail.model.SendEmailResult;
import com.amazonaws.services.simpleemail.model.VerifyEmailAddressResult;
import com.emailservice.email.Email.EmailLogic;
import com.emailservice.email.Model.CustomErrorMessage;
import com.emailservice.email.Model.Email;
import com.emailservice.email.Model.Report;
import com.emailservice.email.Model.ReportAndEmail;
import com.emailservice.email.Report.ReportLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ServiceImpl implements Service {
    @Autowired
    EmailLogic emailLogic;

    @Autowired
    ReportLogic reportLogic;

    public VerifyEmailAddressResult verifyEmail(String emailToVerify) {
        return this.emailLogic.emailToVerify(emailToVerify);
    }

    public SendEmailResult sendEmail(Email email) {
        return this.emailLogic.sendEmail(email);

    }

    public CustomErrorMessage generateAndSendReport(ReportAndEmail reportAndEmail) {
        return emailLogic.sendEmailWithReport(reportAndEmail);
    }
}
