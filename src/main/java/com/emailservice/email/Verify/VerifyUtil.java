package com.emailservice.email.Verify;

import com.emailservice.email.Model.CustomErrorMessage;
import com.emailservice.email.Model.Email;

public class VerifyUtil {
    public static CustomErrorMessage verifyEmailContents(Email email) {
        CustomErrorMessage errorMessage = new CustomErrorMessage();
        if(email.getFromEmail() == null && email.getFromEmail().isEmpty()) {
            errorMessage.setErrorMessage("Invalid From Email");
            errorMessage.setStatusCode(400);
        } else if(email.getToEmail() == null && email.getToEmail().isEmpty()) {
            errorMessage.setErrorMessage("Invalid Get Email");
            errorMessage.setStatusCode(400);
        }

        return null;
    }

}
